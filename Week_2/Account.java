package Week_2;

public class Account {

    public int accountId;
    public double deposit;
    public double withdraw;
    public double balance;
    public String accountCurrency;

    public Account(int accountId, double balance, String accountCurrency) {
        this.accountId = accountId;
        this.balance = balance;
        this.accountCurrency = accountCurrency;
    }

    public void deposit(double deposit) {
        this.deposit = deposit;
        balance += deposit;
        if (balance < 0)
            System.out.println("You don't have enough money for this transaction.");
        else if (balance == 0)
            System.out.println("Your new balance equal to " + balance);
        else
            System.out.println("New Balance: " + balance);
    }

    public void withdraw(double withdraw) {
        this.withdraw = withdraw;

        if (balance < withdraw) {
            System.out.println("You don't have enough money for this transaction.");
        }
        else if (balance == 0) {
            balance -= withdraw;
            System.out.println("Your new balance equal to " + balance);
        }
        else {
            balance -= withdraw;
            System.out.println("New Balance: " + balance);
        }

    }

    public void report() {
        System.out.println("Account_" + accountId + ":\n" +
                "Deposit: " + deposit + "\n" +
                "Withdraw: " + withdraw + "\n" +
                "New Balance: " + balance + " " + accountCurrency);
        System.out.println("====================================");
    }
}
