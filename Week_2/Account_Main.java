package Week_2;

public class Account_Main {

    public static void main(String[] args) {
        Account account1 = new Account(1,10000,"USD");

        account1.deposit(500);
        account1.withdraw(300);
        account1.withdraw(20000);

        account1.report();

        Account account2 = new Account(2, 30000, "TRY");

        account2.deposit(500);
        account2.withdraw(300);
        account2.withdraw(20000);

        account2.report();
    }
}
