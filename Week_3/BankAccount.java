package Week_3;

public class BankAccount {
    private int number;
    private double balance;
    private String currency;
    private double interestRate;

    public BankAccount(int number, double balance, String currency) {
        this.number = number;

        if (balance > 0)
            this.balance = balance;
        else
            this.balance = 0;

        interestRate = 0;
        checkSetCurrency(currency);
    }

    public BankAccount(int number, String currency) {
        this.number = number;
        this.balance = 0;
        interestRate = 0;

        checkSetCurrency(currency);
    }

    public BankAccount(int number) {
        this(number,0,"TL");
        interestRate = 0;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public void checkSetCurrency(String currency) {
        if (currency.equals("USD"))
            this.currency = currency;
        else {
            this.currency = "TL";
        }
    }

    public void setCurrency(String currency) {
        if (this.currency.equals("TL") && currency.equals("USD"))
            balance = balance / 7.52;
        if (this.currency.equals("USD") && currency.equals("TL"))
            balance = balance * 7.52;
        if (currency.equals("TL") ||currency.equals("USD"))
            this.currency = currency;
    }

    public int getNumber() {
        return number;
    }

    public double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }

    public double getInterestRate() {
        return interestRate;
    }
}
