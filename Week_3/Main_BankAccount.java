package Week_3;

public class Main_BankAccount {

    public static void main(String[] args) {
        BankAccount bankAccount1 = new BankAccount(1, 100, "TL");
        bankAccount1.setCurrency("USD");
        System.out.println("Account_1:");
        System.out.println("Balance: " + bankAccount1.getBalance());
        System.out.println("Currency: " + bankAccount1.getCurrency());

        bankAccount1.setInterestRate(0.02);
        System.out.println("Interest Rate: " + bankAccount1.getInterestRate());
        System.out.println("===========================");
    }

}
