package Week_5;

public class Account {
    private int number;
    private double balance;
    private String currency;

    // Constructor
    public Account(int number, double balance, String currency) {
        this.number = number;
        if (balance > 0)
            this.balance = balance;
        else
            this.balance = 0;
        checkSetCurrency(currency);
    }

    public Account(int number, String currency) {
        this(number,0,currency);
    }

    public Account(int number) {
        this(number,0,"TL");
    }

    private void checkSetCurrency(String currency) {
        if (currency.equals("USD"))
            this.currency = currency;
        else if (currency.equals("TL"))
            this.currency = "TL";
        else {
            System.out.println("This currency is not valid in our bank!");
            this.currency = null;
        }
    }

    public void setCurrency(String currency) {
        if (this.currency.equals("TL") && currency.equals("USD"))
            balance /= 8.17;
        else if (this.currency.equals("USD") && currency.equals("TL"))
            balance *= 8.17;
        if (currency.equals("TL") || currency.equals("USD"))
            this.currency = currency;
    }

    public void deposit(double money) {
        if (money > 0) {
                balance += money;
                System.out.println(money + " " + currency + "have been deposited.");
                System.out.println("The balance is " + balance + " " + currency);
            } else {
            System.out.println("The amount should be positive.");
        }
    }

    public void withdraw(double money) {
        if (money > 0) {
            if (balance < money)
                System.out.println("Account does not have " + money + " " + currency);
            else {
                balance -= money;
                System.out.println(money + " " + currency + " have been withdrawn.");
                System.out.println("Remaining balance is " + balance + " " + currency);
            }
        } else {
            System.out.println("The amount should be positive.");
        }
    }

    public void report() {
        System.out.println("Account_" + number + "\n" +
                "Balance: " + balance + " " + currency);
    }

    public String toString() {
        return "Account_" + number + ": " + balance + " " + currency;
    }

    public int getNumber() {
        return number;
    }

    public double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
