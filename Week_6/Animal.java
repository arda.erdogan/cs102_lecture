package Week_6;

public class Animal {
    private String name;
    private String color;

    public String toString() {
        return "Hi, my name is " + this.name + ". I'm " + this.color + ".";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}
