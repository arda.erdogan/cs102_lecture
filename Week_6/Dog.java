package Week_6;

public class Dog extends Animal{
    public Dog(String name) {
        setName(name);
        setColor("red");
    }

    public String speak() {
        return "Woof woof!";
    }

    public String toString() {
        return super.toString() + "\nI am a dog and I woof woof.";
    }
}
