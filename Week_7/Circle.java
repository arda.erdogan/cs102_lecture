package Week_7;

public class Circle extends Shape implements Planer{
    private float radius;

    public Circle(int x, int y, float radius) {
        super(x,y);
        this.radius = radius;
    }

    public float getArea() {
        return radius * radius * (float) Math.PI;
    }

    public float getPerimeter() {
        return 2 * (float) Math.PI * radius;
    }
}
