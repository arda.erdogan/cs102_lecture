package Week_7;

public class Main {

    public static void main(String[] args) {

        Shape rectangle = new Rectangle(0,10,10,5);
        Shape circle = new Circle(10,10,5);

        System.out.println("Area of Rectangle: " + rectangle.getArea());
        System.out.println("Area of Circle: " + circle.getArea());

        System.out.println("Perimeter of Rectangle: " + rectangle.getPerimeter());
        System.out.println("Perimeter of Circle: " + rectangle.getPerimeter());

        System.out.println(rectangle instanceof Circle);
        System.out.println(circle instanceof Rectangle);
        System.out.println(circle instanceof Circle);
        System.out.println(rectangle instanceof Rectangle);
    }

}
