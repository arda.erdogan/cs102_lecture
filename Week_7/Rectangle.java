package Week_7;

public class Rectangle extends Shape implements Planer{

    private float width;
    private float height;

    public Rectangle (int x, int y, float width, float height) {
        super(x,y);
        this.width = width;
        this.height = height;
    }

    public float getArea() {
        return width * height;
    }

    public float getPerimeter() {
        return 2 * (width * height);
    }
}
