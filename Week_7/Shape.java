package Week_7;

public abstract class Shape implements Planer{
    public abstract float getPerimeter();
    private int x;
    private int y;

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
